# Star Citizen Diff

# Latest: sc-alpha-3.18.0-8420577
{- 0 removals -}, {+ 0 additions +}, 14 changes

### How to use this repository

`starcitizen_diff` is is a git repository tracking the differences between version of Star Citizen. Each version is
it's own commit, and can be identified by the commit message.

[OVERVIEW.md][] is generated for each commit as well, which provides a quick, no tools glimpse into what
has changed from the version you're viewing, to the previous version (which will be denoted in the OVERVIEW as well).

The changes can be viewed directly in gitlab by using the `View Changes`
links which will take you to GitLab's comparison tool. Files that are in (or can be converted to) human readable
formats are generated directly in the diff branch providing direct file diff comparisons. For data files,
metadata is stored in its place as way to track what file changed and how.

My preferred method of viewing the changes is to use a Git GUI tool like [fork](https://git-fork.com/) (the free
evaluation is good enough for this task). Clone this repository locally using the `Clone` button above and open the
repository with fork. Select the commit for the version you'd like to inspect and use the changes tab on the bottom of
fork to see all of the files that have changed. Selecting a file in the changes pane will show you the difference for
that file.

#### How it works

The commits are created by generating an inventory of every file in the Star Citizen game folder, including files
located inside the `Data.p4k` archive and DataCoreObjects extracted from the DataCore (Game.dcb in the p4k). Each
entry in the inventory includes the full path of the file as well as a cryptographic hash of the file (using xxhash for
speed). Once the inventories have been generated for the two versions of SC, the differences are compared. Using these
differences a new branch is created, and a file is generated for each change with the contents (or metadata) from the
starting SC version and committed using its version label. The files are removed and the changes are then used to
generate files from the second SC version and committed using its version label. Finally, an overview is generated and
the branch is pushed to this repository. Many DataCoreObjects use the same filename, so all DataCoreObjects have the
id (a GUID) added to the filename to ensure it does not overwrite other DCO and is tracked properly.

### History:

* [sc-alpha-3.18.0-8417663](../../../tree/3daa2c99fec)
  * {- 0 removals -}, {+ 0 additions +}, 15 changes
* [sc-alpha-3.18.0-8412394](../../../tree/1b55cb67aa0)
  * {- 0 removals -}, {+ 0 additions +}, 16 changes
* [sc-alpha-3.18.0-8411181](../../../tree/ec9d87b9fb3)
  * {- 0 removals -}, {+ 0 additions +}, 14 changes
* [sc-alpha-3.18.0-8410680](../../../tree/19cd5813a11)
  * {- 3969 removals -}, {+ 5 additions +}, 914 changes
* [sc-alpha-3.18.x-8389318](../../../tree/69ffef92ab2)
  * {- 0 removals -}, {+ 0 additions +}, 45 changes
* [sc-alpha-3.18.x-8387153](../../../tree/daabd179eb5)
  * {- 0 removals -}, {+ 0 additions +}, 35 changes
* [sc-alpha-3.18.x-8385683](../../../tree/a19227450a7)
  * {- 1 removals -}, {+ 5 additions +}, 17 changes
* [sc-alpha-3.18.x-8384444](../../../tree/572dc9d9cf6)
  * {- 0 removals -}, {+ 0 additions +}, 14 changes
* [sc-alpha-3.18.x-8383510](../../../tree/5014f589444)
  * {- 2 removals -}, {+ 0 additions +}, 28 changes
* [sc-alpha-3.18.x-8382259](../../../tree/4e0311eaed4)
  * {- 0 removals -}, {+ 0 additions +}, 15 changes
* [sc-alpha-3.18.x-8381034](../../../tree/405d610dac8)
  * {- 0 removals -}, {+ 1 additions +}, 33 changes
* [sc-alpha-3.18.x-8380401](../../../tree/972b929d0d4)
  * {- 0 removals -}, {+ 0 additions +}, 14 changes
* [sc-alpha-3.18.x-8378932](../../../tree/071ab147302)
  * {- 0 removals -}, {+ 0 additions +}, 15 changes
* [sc-alpha-3.18.x-8378122](../../../tree/3926579902b)
  * {- 0 removals -}, {+ 0 additions +}, 53 changes
* [sc-alpha-3.18.x-8376002](../../../tree/214dc56e4c5)
  * {- 0 removals -}, {+ 1 additions +}, 416 changes
* [sc-alpha-3.18.x-8374786](../../../tree/e2de4907ee8)
  * {- 0 removals -}, {+ 24 additions +}, 27 changes
* [sc-alpha-3.18.x-8373877](../../../tree/b7148fc20d9)
  * {- 3 removals -}, {+ 16 additions +}, 195 changes
* [sc-alpha-3.18.x-8365461](../../../tree/a2c93061fdb)
  * {- 9698 removals -}, {+ 32381 additions +}, 70848 changes
* [sc-alpha-3.18.x-8364000](../../../tree/42d3b0736ca)
  * {- 624 removals -}, {+ 1176 additions +}, 16271 changes
* [sc-alpha-3.17.x-8338165](../../../tree/265b4f37fc8)
  * {- 32261 removals -}, {+ 9699 additions +}, 70847 changes
* [sc-alpha-3.18.x-8328236](../../../tree/d3124c21510)
  * {- 8 removals -}, {+ 195 additions +}, 11294 changes
* [sc-alpha-3.18.x-8323027](../../../tree/035a8a2db39)
  * {- 0 removals -}, {+ 27 additions +}, 119 changes
* [sc-alpha-3.18.x-8322192](../../../tree/7bbbd2015da)
  * {- 22 removals -}, {+ 185 additions +}, 66 changes
* [sc-alpha-3.18.x-8321451](../../../tree/6a18548d710)
  * {- 0 removals -}, {+ 213 additions +}, 176 changes
* [sc-alpha-3.18.x-8319689](../../../tree/68ec37b37a1)
  * {- 9601 removals -}, {+ 31264 additions +}, 70280 changes
* [sc-alpha-3.17.x-8288900](../../../tree/06e48d51534)
  * {- 219 removals -}, {+ 9612 additions +}, 1749 changes
* [sc-alpha-3.17.x-8240506](../../../tree/455ef8241a9)
  * {- 99115 removals -}, {+ 417 additions +}, 130 changes
* [sc-alpha-3.17.x-8186206](../../../tree/f11f52f87fd)
  * {- 0 removals -}, {+ 2 additions +}, 15 changes


---

This project is not endorsed by or affiliated with the Cloud Imperium or Roberts Space Industries group of companies.
All game content and materials are copyright Cloud Imperium Rights LLC and Cloud Imperium Rights Ltd..  Star Citizen®,
Squadron 42®, Roberts Space Industries®, and Cloud Imperium® are registered trademarks of Cloud Imperium Rights LLC.
All rights reserved.

[OVERVIEW.md]: ./OVERVIEW.md


Latest: sc-alpha-3.18.0-8420577
{- 0 removals -}, {+ 0 additions +}, 14 changes



# Modified

- [.LooseFiles.txt](./.LooseFiles.txt)#1e0479dce8ca9bad1cb78e78e867ba93a402c8ce)
- [Bin64/CIGSocial.dll](./Bin64/CIGSocial.dll)#e6a57ea242e719c7265751802f1e55f6db53b220)
- [Bin64/StarCitizen.exe](./Bin64/StarCitizen.exe)#33447ad0b57744cd242ec8a49861d373c2596d38)
- [build_manifest.id](./build_manifest.id)#ae6094eb26d3afb15671ed44b52ad129d5358a22)
- [Data.p4k/Data/Animations/Characters/Human/female_v2/vault_mantle.dba](./Data.p4k/Data/Animations/Characters/Human/female_v2/vault_mantle.dba)#526d18a8a754c71b31bbcaa6888d512dc979eff8)
- [Data.p4k/Data/Animations/Characters/Human/male_v7/force_reactions.dba](./Data.p4k/Data/Animations/Characters/Human/male_v7/force_reactions.dba)#30d07baacaaeeb254ac28eb55e9891c1c484e686)
- [Data.p4k/Data/Animations/Characters/Human/male_v7/vault_mantle.dba](./Data.p4k/Data/Animations/Characters/Human/male_v7/vault_mantle.dba)#8cb288f3ff601c3202bd4872569f003bc34fde6f)
- [Data.p4k/Data/Animations/DirectionalBlends.img](./Data.p4k/Data/Animations/DirectionalBlends.img)#f6921efee19c8d6516405121523542a17cbc5b27)
- [Data.p4k/Engine/f_puewc_shaders.id](./Data.p4k/Engine/f_puewc_shaders.id)#0d03f451078cd31bc60ab2481f5bb5ad5d3cef46)
- [Data.p4k/Engine/ShaderCache.pak](./Data.p4k/Engine/ShaderCache.pak)#c3cd8e4877b6ecdd7aa6bf706768569df281c51d)
- [EasyAntiCheat/Certificates/base.bin](./EasyAntiCheat/Certificates/base.bin)#7903357a5d90425f40bad34eb3a42c7f4dd51556)
- [f_win_game_client_release.id](./f_win_game_client_release.id)#917d834fa4b9c8cc93be2009c1b38b7b9d9a9fac)
- [Tools/Public/CrashHandler.exe](./Tools/Public/CrashHandler.exe)#8ec06004a905e24ff2981376f6a2c02b7f62bb70)
___
1 [filtered](./FILTERED.md) changes not shown. <sub>(files ending with .wem, .aim)<sub>